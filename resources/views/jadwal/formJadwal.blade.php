@extends('layout')
@section('title', 'Tambah Jadwal')
@section('content')
@if(count($errors) > 0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form method="post">
 <div class="form-group row">
  <div class="col-sm-12">
    <label class="control-label" for="tanggal">Pilih Tanggal</label>
    <?php 
    date_default_timezone_set("Asia/Makassar");
    if(isset($data->tanggal_S)){      
      $date_S = date('Y-m-d H:i',$data->tanggal_S);
      $date_E = date('Y-m-d H:i',$data->tanggal_E);
    }
    ?>
    <input type="text" class="form-control" id="tanggal" placeholder="Isi Tanggal" name="tanggal"
    value="{{isset($data->tanggal_S) ? $date_S.' - '.$date_E : ' ' }}">
  </div>
</div>
@if( session('auth')->level == 3)
<div class="form-group row">
  <div class="col-sm-12">
    @foreach($list as $mylist)
    <span class="label label-default">{{ $mylist->nik }}</span>
    @endforeach
  </div>
</div>
@endif
<div class="form-group row">
  <div class="col-sm-6 mb-3 mb-sm-0">
    <select style="width: 100%" class="form-control form-control-user" id="nik" multiple="multiple" name="nik[]">

    </select>
  </div>
</div>
<div class="form-group">
  <input type="text" class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan"
  value="{{isset($data->keterangan) ? $data->keterangan : ' ' }}"></textarea>
</div>

<button type="submit" class="btn btn-success btn-block"><span class="fas fa-save fa-sm text-white-50"></span> Save</button>
</form>
@endsection
@section('myjs')
<script>
  $(function() {

    $('#tanggal').daterangepicker({
     timePicker: true,
     timePickerIncrement: 30,
     timePicker24Hour: true,
     locale: {
      format: 'YYYY-MM-DD HH:mm'
    }
  });

    if({{ session('auth')->level }} == 3){
      console.log('tes');
      $('#nik_from_tl').attr('id', 'nik_from_tl');
      $('#tanggal').attr('disabled', true);
    }

    $('#nik').select2({
      placeholder: "Masukkan NIK Peserta Atau Nama",
      minimumInputLength: 3,
      ajax: { 
        url: "/search/nik",
        dataType: 'json',
        delay: 350,
        data: function (params) {
          return {
            searchTerm: params.term,
          };
        },
        processResults: function (response) {
          return {
            results: response
          };
        },
        cache: true,
        success: function(data) {
          console.log(data);
        }
      }
    });

    $('#nik_from_tl').select2({
      placeholder: "Masukkan NIK Peserta Atau Nama",
      minimumInputLength: 3,
      ajax: { 
        url: "/search/tl_search/{{ session('auth')->nik }}/{{ isset($data->id) ? $data->id : '' }}",
        dataType: 'json',
        delay: 350,
        data: function (params) {
          return {
            searchTerm: params.term,
          };
        },
        processResults: function (response) {
          return {
            results: response
          };
        },
        cache: true,
        success: function(data) {
          console.log(data);
        }
      }
    });
    
    var array_peserta = <?=json_encode($peserta); ?>;
    console.log(array_peserta[0]);
    if( array_peserta != null){
      for (var i = 0; i < array_peserta.length; ++i) {
       $("#nik").append("<option value='"+array_peserta[i].nik+"'selected>"+array_peserta[i].nama+" ("+array_peserta[i].nik+")</option>");
       $('#nik').trigger('change');
     }
   }
   if( array_peserta != null){
    for (var i = 0; i < array_peserta.length; ++i) {
     $("#nik_from_tl").append("<option value='"+array_peserta[i].nik+"'selected>"+array_peserta[i].nama+" ("+array_peserta[i].nik+")</option>");
     $('#nik_from_tl').trigger('change');
   }
 }
});


</script>
@endsection