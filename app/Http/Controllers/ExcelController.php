<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\support\facades\session;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\UsersExport;


class ExcelController extends Controller
{
	public function show_download_des(Request $req){
		$date1 = strtotime($req->start);
		$date2 = strtotime($req->end);
		if($req->status[0] == 'excel'){
			return redirect("/download/excel/$date1/$date2");
		}
	}
	public function download_excel($date1, $date2){

		return Excel::download(new UsersExport($date1, $date2), 'export.xlsx');

	}
	public function show_download(){
		return view ('admin.excel_Data');
	}

	public function view_upload(){
		return view('nilai.form');
	}

	public function upload_excel(Request $req){
		$input = 'file_foto';
		if($req->hasFile($input)){
			//siapkan folder upload auto
			$path = public_path().'/upload/';
			if (!file_exists($path)){
				if(!mkdir($path, 0775, true))
					return 'gagal menyiapkan folder foto evidence';
			}
			//

			//taroh file ke variable
			$file = $req->file($input);
			try{
				//mendapatkan nama asli file
				$nama = $file->getClientOriginalName();
				//proses uploading
				$moved = $file->move("$path", "$nama");

				//proses update sql kolom 'upload_n' di table 'jadwal'
				DB::table('jadwal')->where('id', '18')->update(['upload_n' => $nama]);
			}
			catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e)
			{
				return 'gagal menyimpan foto evidence'.$id;
			}
		}
		return redirect('/home');
	}



}