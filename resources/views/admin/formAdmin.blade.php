@extends('layout')
@section('content')
@if(count($errors) > 0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form method="post">

  <div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
      <input type="text" class="form-control form-control-user" id="nik" placeholder="Isi NIK" name="nik"
        value="{{ isset($dataadmin->nik)? $dataadmin->nik : '' }}">
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control form-control-user" id="nama" placeholder="Isi nama" name="nama"
        value="{{ isset($dataadmin->nama)? $dataadmin->nama : '' }}">
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
      <input type="text" class="form-control form-control-user" id="username" placeholder="Isi username" name="username"
        value="{{ isset($dataadmin->username)? $dataadmin->username : '' }}">
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control form-control-user" id="password" placeholder="Isi password" name="password"
        value="{{ isset($dataadmin->password)? $dataadmin->password : '' }}">
    </div>
  </div>
  <button type="submit" class="btn btn-success btn-block"><span class="fas fa-save 
   fa-sm text-white-50"></span> Save</button>
</form>
@endsection