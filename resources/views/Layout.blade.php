<!DOCTYPE html>

<html>
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

 <title>@yield('title')</title>
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
 <!-- Core stylesheets -->
 <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css">
 <link href="/css/pixeladmin.min.css" rel="stylesheet" type="text/css">
 <link href="/css/widgets.min.css" rel="stylesheet" type="text/css">

 <!-- Theme -->
 <link href="/css/themes/clean.min.css" rel="stylesheet" type="text/css">

 <!-- Pace.js -->
 <script src="/pace/pace.min.js"></script>
 <style type="text/css">
 .red{
  color:red;
}

</style>
@yield('css')
</head>
<body>
  <nav class="px-nav px-nav-left">
    <button type="button" class="px-nav-toggle" data-toggle="px-nav">
      <span class="px-nav-toggle-arrow"></span>
      <span class="navbar-toggle-icon"></span>
      <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
    </button>

    <ul class="px-nav-content">
      <li class="px-nav-item">
        <li class="px-nav-item"><a href="/home"><span class="px-nav-label">Jadwal</span></a></li>
        @if((session('auth')->level == 1) || (session('auth')->level == 3))
        <li class="px-nav-item"><a href="/peserta"><span class="px-nav-label">Peserta</span></a></li>
        @endif
        @if((session('auth')->level == 1))
        <li class="px-nav-item"><a href="/Team_L"><span class="px-nav-label">Team Leader</span></a></li>
        <li class="px-nav-item"><a href="/unduh"><span class="px-nav-label">Download Data Absensi FA</span></a></li>
        @endif
      </li>
    </ul>
  </nav>


  <nav class="navbar px-navbar">
    <!-- Header -->
    <div class="navbar-header">

      <a class="navbar-brand px-demo-brand" href="/">INDIHOME</a>
    </div>


    <!-- Navbar togglers -->
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-demo-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <ul class="nav navbar-nav navbar-right">

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <span href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            {{ session('auth')->nama }} 
          </span>
        </a>
        <ul class="dropdown-menu">
          <li class="divider"></li>
          <li><a href="/logout"><i class="dropdown-icon fa fa-power-off"></i>&nbsp;&nbsp;Log Out</a></li>
        </ul>
      </li>

    </ul>
  </nav>

  <div class="px-content">
    <ol class="breadcrumb page-breadcrumb">
      <li><a href="home">Home</a></li>
      <li class="active">Fiber Academy</li>
    </ol>

    <div class="page-header">
      <div class="row">
        <div class="col-md-4 text-xs-center text-md-left text-nowrap">
          <h1><i class="page-header-icon ion-ios-pulse-strong"></i>Fiber Academy</h1>
        </div>

        <hr class="page-wide-block visible-xs visible-sm">

        <!-- Spacer -->
        <div class="m-b-2 visible-xs visible-sm clearfix"></div>

        <form action="" class="page-header-form col-xs-12 col-md-4 pull-md-right">
        </form>
      </div>
    </div>
    <div class="container-fluid">
      <div class="col-md-12" style="padding:10px;">

        @yield('content')

       
      </div>
    </div>
  </div>

  
  
  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- Load jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- Core scripts -->
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>

  <!-- Your scripts -->

  @yield('myjs')
  <script type="text/javascript">
  </script>

</body>
</html>
