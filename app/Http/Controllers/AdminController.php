<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\support\facades\session;

class AdminController extends Controller
{
	public function listAdmin () {
		$fa_absen = DB::table('team_leader')->get();
		//dd($fa_absen);
		return view ('admin.listAdmin',compact('fa_absen'));
	}
	public function admininput($id) {
		$dataadmin = DB::table('team_leader')->where('id', $id)->first();
		return view ('admin.formAdmin',compact('dataadmin'));
		
	}
	public function adminsave(request $req, $id) {
		
		$messages = [
			'nik.required' => 'nik tidak boleh kosong',
			'nama.required' => 'Harus Diisi, jangan kosong',
			'max' => ':attribute harus diisi maksimal :max
			karakter !!!',
		];
		$this->validate($req,[
			'nik' => 'required|numeric',
			'nama' => 'required|min:5|max:20',
			'username' => 'required',
			'password' => 'required'
		], $messages);
		$exists=DB::table('team_leader')->where('id',$id)->first();
		if($exists){
			DB::table('team_leader')->where('id', $id)->update([
			"nik"		=> $req->nik,
			"nama"		=> $req->nama,
			"username"		=> $req->username,
			"password"		=> $req->password,
		]);
	}else{
		
		DB::table('team_leader')->insert([
			"nik"		=> $req->nik,
			"nama"		=> $req->nama,
			"username"		=> $req->username,
			"password"		=> $req->password
		]);
	}
		return redirect('/Admin');
		
	}
	public function admindelete($id){
		DB::table('team_leader')->where('id',$id)->delete();
		return redirect('/Admin');
	}


	public function login(request $req){
		//dd($req);
		$exists = DB::table('admin')	
		->where('username', $req->username)
		->where('password', $req->password)->first();
	
	if($exists){
		Session::put('auth',$exists);
		return redirect('/home');
	}else{
		return redirect('/');
		}
	}
	public function logout(){
		Session::forget('auth');
		return redirect('/');
	}
	
}
