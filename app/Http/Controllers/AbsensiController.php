<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Session;
use Redis;
use DB;

class AbsensiController extends Controller
{

  public function presensiFromQR($id_jadwal, $time, UserController $usercontroller){
   $auth = session('auth');
    	$datetime1 = strtotime(date('Y-m-d H:i:s', $time));//start time
      $datetime2 = time();//end time
      $diff = $datetime2 - $datetime1;
      if($diff<80){
      	$redis = Redis::connection();
        $data = [
          'nik' 		=> $auth->nik,
          'id_jadwal'     => $id_jadwal,
          'nama'		=> $auth->nama,
          'Batas'		=> strtotime(date('Y-m-d').' 08:00:00')
        ];
        $verify = DB::table('peserta')->where([['id_jadwal', $id_jadwal], ['nik', $auth->nik]])->first();
        if(isset($verify)){
          $cek = DB::SELECT("SELECT * FROM save_absen WHERE id_jadwal = $id_jadwal AND nik = $auth->nik AND DATE_FORMAT($time, '%Y-%m-%d') = DATE_FORMAT(date, '%Y-%m-%d')");
          if(!$cek){
            $data = DB::table('save_absen')->insert($data);

            $redis->publish('message', json_encode($usercontroller->data_front_page()));

            $status =['type' => 'success', 'text' => 'Berhasil!'];
          }else{
           $status =['type' => 'warning', 'text' => 'Done Already!'];
        }
      }else{
         $status =['type' => 'warning', 'text' => 'Tidak Terjadwal!'];
      }
    }else{
      $status = ['type' => 'danger', 'text' => 'QR Code expired, Coba Scan Lagi!'];
    }
    return redirect('/')->with($status);
  }

}
