<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
	return view('login');
});

Route::post('/login','UserController@login');
Route::group(['middleware' =>'CustomAuth'],function(){
	//user
	Route::get('/conf/data/{nik}/{date}/{id}/{date2}','UserController@confirmation');
	Route::get('/', 'UserController@front_page');
	Route::get('/pdf', 'PDfController@fa_absen_pdf');
	Route::get('/peserta', 'UserController@list');
	Route::get('/peserta/{id}','UserController@input');
	Route::post('/peserta/{id}','UserController@save');
	Route::get('/deleteuser/{id}','UserController@delete');
	Route::get('/peserta', 'UserController@list');
	Route::get('/Team_L', 'UserController@list_tl');
	Route::get('/Team_L/{id}', 'UserController@input');
	Route::post('/Team_L/{id}', 'UserController@save_TL');
	Route::get('/search/tl/nik','UserController@search_tl');
	//pdf
	Route::get('/pdf', 'PDfController@fa_absen_pdf');
	//excel
	Route::post('/unduh','ExcelController@show_download_des');
	Route::get('/unduh', 'ExcelController@show_download');
	Route::get('/download/excel/{data1}/{data2}', 'ExcelController@download_excel');
	Route::get('/uploadNilai', 'ExcelController@view_upload');
	Route::post('/uploadNilai', 'ExcelController@upload_excel'); 
	//jadwal
	Route::get('/search/tl_search/{nik}/{id}','JadwalController@search_tl_slave');
	Route::get('/search/nik', 'JadwalController@search_nik');
	Route::get('/home', 'JadwalController@jadlist');
	Route::get('/home/{id}','JadwalController@jadinput');
	Route::post('/home/{id}','JadwalController@jadsave');
	Route::get('/deletejadwal/{id}','JadwalController@jaddelete');

	Route::get('/absen/{id}/{time}','AbsensiController@presensiFromQR');
	
});
	Route::get('/logout','UserController@logout');
