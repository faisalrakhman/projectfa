@section('title', 'Jadwal')
@extends('layout')
@section('content')
@if(Session::has('type'))
<ul>
	<div class="alert alert-{{session::get('type')}}">
		<li>{!! session::get('text') !!}</li>
	</div>
</ul>
@endif
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Jadwal Pelatihan</h1>
	@if((session('auth')->level == 1))
	<a style="margin-bottom: 15px;" href="/home/input" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
		<i class="fas fa-plus fa-sm text-white-50"></i> Tambah </a>
		@endif
	</div>
	@if(!empty($fa_absen[0]))
	@foreach($fa_absen as $no => $data)
	<div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>#</th>
					<th>Tanggal Mulai</th>
					<th>Tanggal Berakhir</th>
					<th>Peserta</th>
					<th>Keterangan</th>
					@if(session('auth')->level == 1)
					<th style="text-align: center;" colspan="2">Action</th>
					@endif
					@if(session('auth')->level == 3)
					<th>Action</th>
					@endif
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ ++$no }}</td>
					<td>{{ date('Y-m-d H:i:s', $data->tanggal_S) }} </td>
					<td>{{ date('Y-m-d H:i:s', $data->tanggal_E) }} </td>
					<td>
						<ul>
							@foreach($data->data as $mydata)
							<li>{{ $mydata->nama }} ({{ $mydata->nik }})</li>
							@endforeach
						</ul>
					</td>
					<th>{{ $data->keterangan }}</th>
					@if((session('auth')->level == 1) || (session('auth')->level == 3))
					<td align="center">
						<a target="_blank" href="/home/{{ $data->id}}" class="btn-info btn-sm"><i class="fas fa-edit fa-sm text-white-50"></i>{{ (session('auth')->level ==1 ? 'Edit' : ( session('auth')->level == 3 ? 'Tambah Peserta' : '')) }} </a> </td>
						@endif
						@if((session('auth')->level == 1))
						<td align="center">
							<a target="_blank" href="/deletejadwal/{{ $data->id}}" class="btn-danger btn-sm "><i class="fas fa-trash fa-sm text-white-50"></i> Delete</a>
						</td>
						@endif
					</tr>
				</tbody>
			</table>
		</div>
		@endforeach
		@else
		Jadwal belum tersedia
		@endif
		@endsection