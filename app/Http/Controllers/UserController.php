<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\support\facades\session;
date_default_timezone_set("Asia/Makassar");

class UserController extends Controller
{
	public function list () {
// dd(session('auth'));
		if(session('auth')->level == 1){
			$fa_absen = DB::table('login')->where('level',2)->get();
		}elseif(session('auth')->level == 3){
			$fa_absen = DB::table('login')->where([['level', 2], ['nik_tl', session('auth')->nik]])->get();
		}
//dd($fa_absen);
		return view ('user.list',compact('fa_absen'));
	}

	public function list_tl () {
// dd(session('auth'));
		$fa_absen = DB::table('login')->where('level',3)->get();
//dd($fa_absen);
		return view ('user.list',compact('fa_absen'));
	}

	public function data_front_page(){
		$sdhabsen = DB::select("SELECT jadwal.id, peserta.id_jadwal, save_absen.id as id_absen, save_absen.id_jadwal, jadwal.tanggal_S, jadwal.tanggal_E, peserta.nik as nik_peserta, save_absen.batas, save_absen.nik as nik_absen, save_absen.date, peserta.agreement, login.nik, login.nama FROM jadwal LEFT JOIN peserta ON jadwal.id = peserta.id_jadwal LEFT JOIN save_absen ON peserta.id_jadwal = save_absen.id_jadwal LEFT JOIN login ON peserta.nik = login.nik WHERE tanggal_E > UNIX_TIMESTAMP(DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i')) AND agreement = 0 AND save_absen.nik = peserta.nik order by save_absen.date DESC");
		$blmabsen = DB::select("SELECT jadwal.id, peserta.id_jadwal, save_absen.id as id_absen, save_absen.id_jadwal, jadwal.tanggal_S, jadwal.tanggal_E, peserta.nik as nik_peserta, save_absen.batas, save_absen.nik as nik_absen, save_absen.date, peserta.agreement, login.nik, login.nama FROM jadwal LEFT JOIN peserta ON jadwal.id = peserta.id_jadwal LEFT JOIN save_absen ON peserta.nik = save_absen.nik LEFT JOIN login ON peserta.nik = login.nik WHERE tanggal_E > UNIX_TIMESTAMP(DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i')) AND agreement = 0 AND save_absen.id_jadwal IS NULL order by save_absen.date DESC");
		$list =  array_merge($sdhabsen, $blmabsen);
		$jadwal =  DB::table('jadwal')->where('tanggal_E', '>', strtotime(date('Y-m-d H:i')))->orderBy('tanggal_S', 'ASC')->orderBy('tanggal_E', 'ASC')->get();
		if(!empty($jadwal[0])){
			foreach($jadwal as $no => $value){
				$data[$no] = $value;
				foreach($list as $key => $another_v){
					if ($another_v->id == $value->id){
						$data[$no]->list_list[] = $another_v;
					}
				}
			}
		}else{
			$data = [];
		}
		return $data;
	}

	public function front_page(){
		$data = $this->data_front_page();
		return view('absen.list', compact('data'));
	}

	public function input($id) {
		$data = DB::table('login')->where('id', $id)->first();
		return view ('user.form',compact('data'));

	}

	public function save_TL(request $req, $id) {
		$messages = [
			'nama.required' => 'Harus Diisi, jangan kosong',
			'nik.required' => 'nik tidak boleh kosong',
			'max' => ':attribute harus diisi maksimal :max
			karakter !!!',
		];
		$this->validate($req,[
			'nama' => 'required|min:5|max:20',
			'nik' => 'required|numeric',
			'unit' => 'required',
			'email' => 'required',
		], $messages);
		$exists=DB::table('login')->where([['id', $id],['level',2]])->first();
		if($exists){
			DB::table('login')->where([['id', $id],['level',2]])->update([
				"nama"		=> $req->nama,
				"level"		=> 3,
				"nik"		=> $req->nik,
				"unit"		=> $req->unit,
				"email"		=> $req->email,
			]);
			$pesan = 'Data Telah Berhasil Diperbaharui!';
		}else{
			DB::table('login')->insert([
				"nama"			=> $req->nama,
				"level"			=> 3,
				"nik"			=> $req->nik,
				"unit"			=> $req->unit,
				"email"			=> $req->email,
				"password"		=> md5("telkomakses123")
			]);
			$pesan = 'Data Telah Berhasil Diinput!';
		}
		$alert = ['type' => 'success', 'text' => $pesan];
		return redirect('/home')->with($alert);

	}

	public function save(request $req, $id) {
		$messages = [
			'nama.required' => 'Harus Diisi, jangan kosong',
			'nik.required' => 'nik tidak boleh kosong',
			'max' => ':attribute harus diisi maksimal :max
			karakter !!!',
		];
		$this->validate($req,[
			'nama' => 'required|min:5|max:20',
			'nik' => 'required|numeric',
			'unit' => 'required',
			'email' => 'required',
		], $messages);
		$exists=DB::table('login')->where([['id', $id],['level',2]])->first();
		if($exists){
			DB::table('login')->where([['id', $id],['level',2]])->update([
				"nama"		=> $req->nama,
				"level"		=> 2,
				"nik"		=> $req->nik,
				"nik_tl"	=> $req->nik_tl,
				"unit"		=> $req->unit,
				"email"		=> $req->email,
			]);
			$pesan = 'Data Telah Berhasil Diperbaharui!';
		}else{
			DB::table('login')->insert([
				"nama"			=> $req->nama,
				"level"			=> 2,
				"nik"			=> $req->nik,
				"nik_tl"		=> $req->nik_tl,
				"password"		=> md5("telkomakses123"),
				"unit"			=> $req->unit,
				"email"			=> $req->email
			]);
			$pesan = 'Data Telah Berhasil Diinput!';
		}
		$alert = ['type' => 'success', 'text' => $pesan];
		return redirect('/home')->with($alert);

	}

	public function delete($id){
		$recheck= DB::table('jadwal')->leftjoin('peserta', 'jadwal.id', '=', 'peserta.id_jadwal')->where([ ['peserta.nik', $id],['tanggal_E', '>', DB::raw('UNIX_TIMESTAMP(DATE_FORMAT(NOW(), \'%Y-%m-%d %H:%i\'))')] ])->get();
		dd($recheck);
		if($recheck){
			DB::DELETE("DELETE login, peserta FROM login LEFT JOIN peserta ON login.nik = peserta.nik LEFT JOIN jadwal ON peserta.id_jadwal = jadwal.id WHERE login.nik = $id AND tanggal_E > UNIX_TIMESTAMP(DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i')");
		}else{
			DB::table("login")->where('nik', $id)->delete();
		}
		// dd($test);
		$alert = ['type' => 'success', 'text' => 'Data dengan NIK '.$id.' Berhasil Dihapus!'];
		return redirect('/peserta')->with($alert);

	}

	public function confirmation($nik, $date, $id, $date2){
		if(Session::Has('auth')){
			$nik = session('auth')->nik;
			$database = DB::SELECT("SELECT * FROM jadwal LEFT JOIN peserta ON jadwal.id = peserta.id_jadwal WHERE id_jadwal = $id AND peserta.nik = $nik");
			if(isset($database[0])){
				$alert = ['type' => 'success', 'text' => 'Nik '.$nik.' Berhasil Mengkonfirmasi!'];
				return redirect('/home')->with($alert); 
			}else{
				$alert = ['type' => 'danger', 'text' => 'Nik '.$nik.' Tidak Terdaftar Dimanapun!'];
				return redirect('/home')->with($alert); 
			}
		}
	}

	public function search_tl(Request $req){
		if(!isset($req->searchTerm)){
			$value = 'order by name';
			$fetchData = DB::table('login')->where('level', 3)->limit(5)->get();
		}else{ 
			$search = strtoupper($req->searchTerm);
			$fetchData = DB::table('login')->where('level', 3)->where(function($join) use($search) {
				$join->where('nik', 'like', '%'.$search.'%')->orwhere('nama', 'like', '%'.$search.'%');
			})->get();
		}
		if(isset($fetchData[0])){
			foreach ($fetchData as $value) {
				$data[] = ["id" => $value->nik, "text" => $value->nama.' ('.$value->nik.')'];
			}
		}else{
			dd(md5('saatr').''.md5(md5('adds')));
		}
		return \Response::json($data);
	}

	public function login(request $req){
//dd($req);
		$messages = [
			'username.required' => 'Harus Diisi, jangan kosong',
			'password.required' => 'nik tidak boleh kosong',
			
		];
		$this->validate($req,[
			'username' => 'required',
			'password' => 'required',
		], $messages);

			$messages = 'gagal login, silahkan coba lagi!';

		$destory = explode('/', $req->previous);
		$hitung = count($destory);
		$exists = DB::table('login')	
		->where('nik', $req->username)
		->where('password', md5($req->password))->first();

		if($exists){
			Session::put('auth',$exists);
			if($hitung <= 3){
				return redirect('/');
			}else{
				if($destory[3] == 'logout' || $destory[3] == 'login'){
					return redirect('/');
				}else{
					if($destory[3] == 'conf'){
						return redirect('/conf/data/'.$req->username.'/date');
					}else{
						return redirect($req->previous);
					}
				}
			}
		}else{

			return redirect('/');
		}

	}

	public function logout(){
		Session::forget('auth');
		return redirect('/login');
		$pesan = 'Berhasil logout';
	}

}
