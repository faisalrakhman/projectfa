@extends('layout')
@if( Request::segment(1) == 'peserta')
@section('title', 'List User')	
@elseif( Request::segment(1) == 'Team_L')
@section('title', 'List Team Leader')	
@endif
@section('content')
@if(Session::has('type'))
<ul>
	<div class="alert alert-{{session::get('type')}}">
		<li>{!! session::get('text') !!}</li>
	</div>
</ul>
@endif
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Daftar  {{ (Request::segment(1) == 'peserta' ? 'User' : (Request::segment(1) == 'Team_L' ? 'Team Leader' : '')) }}</h1>
	<a style="margin-bottom: 15px;" href="/{{ Request::segment(1) }}/input" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
		<i class="fas fa-plus fa-sm text-white-50"></i> Tambah {{ (Request::segment(1) == 'peserta' ? 'User' : (Request::segment(1) == 'Team_L' ? 'Team Leader' : '')) }}</a>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>#</th>
					<th>Nama</th>
					<th>NIK</th>
					<th>Unit</th>
					<th>Email</th>
					@if(session('auth')->level == 1 || session('auth')->level == 3)
					<th style="text-align: center;" colspan="2">Action</th>
					@endif
				</tr>
			</thead>
			<tbody>
				@foreach($fa_absen as $no => $data)
				<tr>
					<td>{{ ++$no }}</td>
					<td>{{ $data->nama }}</td>
					<td>{{ $data->nik }}</td>
					<td>{{ $data->unit }}</td>
					<td>{{ $data->email }}</td>
					@if(session('auth')->level == 1 || session('auth')->level == 3)
					<td align="center">
						<a href="/{{ Request::segment(1) }}/{{ $data->id}}" class="btn-info btn-sm"><i class="fas fa-edit fa-sm text-white-50">
						</i> Edit </a>
					</td>
					<td align="center">
						<a href="/deleteuser/{{ $data->nik }}" class="btn-danger btn-sm "><i class="fas fa-trash fa-sm text-white-50">
						</i> Delete</a>
					</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endsection
	@section('myjs')
	
	<script>
		$(function() {
			var socket = io('http://localhost:8890');
			socket.on('message', function (data) {
				console.log('ada yg presen');
			});

		});
	</script>
	@endsection