@extends('layout')
@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Daftar Admin</h1>
            <a href="/Admin/input" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-plus fa-sm text-white-50"></i> Register </a>
          </div>
	<div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 <thead>
                    <tr>
                    <th>#</th>
					<th>NIK</th>
					<th>Nama</th>
					<th>Username</th>
					<th>Password</th>
					<th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
			@foreach($fa_absen as $no => $dataadmin)
			<tr>
				<th>{{ ++$no }}</th>
				<th>{{ $dataadmin->nik }}</th>
				<th>{{ $dataadmin->nama }}</th>
				<th>{{ $dataadmin->username }}</th>
				<th>{{ $dataadmin->password }}</th>
				<th><a href="/Admin/{{ $dataadmin->id}}" class="btn-info btn-sm"><i class="fas fa-edit fa-sm text-white-50">
				</i> Edit </a>
				<a href="/deleteadmin/{{ $dataadmin->id }}" class="btn-danger btn-sm "><i class="fas fa-trash fa-sm text-white-50">
				</i> Delete</a></th>
			</tr>
			@endforeach
  </tbody>
</table>
</div>
@endsection