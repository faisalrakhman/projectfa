@extends('layout')
@section('content')
@section('title', 'Download Jadwal')
<form method="post">
	<div class="input-daterange input-group" id="datepicker-range">
		<input type="text" class="form-control" name="start">
		<span class="input-group-addon">to</span>
		<input type="text" class="form-control" name="end">
	</div>
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<button type="submit" name="status[]" value="excel" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
			<i class="fas fa-plus fa-sm text-white-50"></i> Download Excel
		</button>
	</div>
</form>
@endsection
@section('myjs')

<script>
	$(function() {
		$('#datepicker-range').datepicker();
	});
</script>
@endsection

