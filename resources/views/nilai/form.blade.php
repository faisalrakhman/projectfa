@extends('layout')
@section('content')
@section('title', 'Upload Jadwal')
<form method="post" enctype="multipart/form-data">
<div class="form-group">
    <div class="col-sm-12 mb-3 mb-sm-0">
      <input type="file" class="form-control" name="file_foto">
    </div>
  </div>
  <button type="submit" class ="btn btn-success btn-block es"><span class="fas fa-save fa-sm text-white-50"></span> Save</button>
</form>
@endsection
@section('myjs')
<script>
  $(function() {
    
  });
</script>
@endsection

