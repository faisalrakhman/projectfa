<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\support\facades\session;
use Telegram\Bot\Laravel\Facades\Telegram;
date_default_timezone_set("Asia/Makassar");


class JadwalController extends Controller
{
	public function jadlist () {
		$data = DB::table('jadwal')->where('tanggal_E', '>', strtotime(date('Y-m-d H:i')))->orderBy('tanggal_S', 'ASC')->orderBy('tanggal_E', 'ASC')->get();
		if(isset($data[0])){
			$peserta = DB::table('peserta')->leftjoin('login', 'peserta.nik', '=', 'login.nik')->get();
			foreach ($data as $key => $n) {
				$fa_absen[$key] = $n;
				foreach($peserta as $n_p){
					if($n_p->id_jadwal == $n->id){
						$fa_absen[$key]->data[] = $n_p;
					}
				}
			}	
		}
		else {
			$fa_absen[] = '';
		}

		// dd($peserta);
		return view ('jadwal.listjadwal',compact('fa_absen'));
	}

	public function jadinput($id) {
		$data = DB::table('jadwal')->where('id', $id)->first();
		if(!empty($data)){
			if(session('auth')->level == 1){
				$main_data = DB::table('peserta')->leftjoin('login', 'peserta.nik', '=', 'login.nik')->where('peserta.id_jadwal', $id)->get();
				$list = null;
			}elseif(session('auth')->level == 3){
				$main_data = DB::table('peserta')->leftjoin('login', 'peserta.nik', '=', 'login.nik')->where([['peserta.id_jadwal', $id],['login.nik_tl', session('auth')->nik]])->get();
				$list = DB::table('jadwal')->leftjoin('peserta', 'jadwal.id', '=', 'peserta.id_jadwal')->where([['jadwal.id', $id],['dispatch_level', '=', 0]])->get();
			}
			// dd($main_data);
			if(isset($main_data[0])){
				foreach($main_data as $d){
					$get[] = $d->nik;
				}
				$peserta = DB::table('login')->whereIn('nik', $get)->get();
			}else{
				$peserta = null;
			}
		}else{
			$peserta = null;
			$list = null;
		}
		// dd($peserta);
		return view ('jadwal.formjadwal',compact('data', 'peserta', 'list'));
		
	}

	public function search_tl_slave(Request $req, $nik, $id){
		if(!isset($req->searchTerm)){
			$fetchData = DB::table('login')->where('nik_tl', $nik)->limit(5)->get();
		}else{ 
			$search = strtoupper($req->searchTerm);
			$filter_data = DB::table('peserta')->where([['nik', 'like', '%'.$search.'%'],['id_jadwal', $id]])->first();
			if(empty($filter_data)){
				$fetchData = DB::table('login')->where('nik_tl', $nik)
				->where(function($join) use($search) {
					$join->where('nik', 'like', '%'.$search.'%')
					->orwhere('nama', 'like', '%'.$search.'%');
				})->get();
			}else{
				dd(md5('saatr').''.md5(md5('addsss')));
			}
			// dd($fetchData);
		}
		if(isset($fetchData[0])){
			foreach ($fetchData as $value) {
				$data[] = ["id" => $value->nik, "text" => $value->nama.' ('.$value->nik.')'];
			}
		}else{
			dd(md5('saatr').''.md5(md5('adds')));
		}
		return \Response::json($data);
	}

	public function search_nik(Request $req){
		if(!isset($req->searchTerm)){
			$value = 'order by name';
			$fetchData = DB::table('login')->where('level', 2)->limit(5)->get();
		}else{ 
			$search = strtoupper($req->searchTerm);
			$fetchData = DB::table('login')->where('level', 2)->where(function($join) use($search) {
				$join->where('nik', 'like', '%'.$search.'%')
				->orwhere('nama', 'like', '%'.$search.'%')
				->orwhere('nik_tl', 'like', '%'.$search.'%');
			})->get();
		}
		if(isset($fetchData[0])){
			foreach ($fetchData as $value) {
				$data[] = ["id" => $value->nik, "text" => $value->nama.' ('.$value->nik.')'];
			}
		}else{
			dd(md5('str').''.md5(md5('as')));
		}
		return \Response::json($data);
	}

	public function jadsave(request $req, $id) {
		$message = '';
		$hasil = 0;
		$msg = '';
		$msg .= "Dikirim dari Fiber Academy \n";

		if(!empty($req->nik)){
			$nik_raw = array_map('intval', $req->nik);
			$count = count($req->nik);
		}else{
			$nik_raw = [];
			$count = 0 ;
		}
		$exists=DB::table('jadwal')->where('id',$id)->first();
		$destroy = explode(" ", $req->tanggal);
		if($exists){
			if(session('auth')->level == 1){
				$messages = [
					'tanggal.required' => 'tanggal tidak boleh kosong',
					'nik.required' => 'nama tidak boleh kosong',
				];

				$this->validate($req,[
					'nik' => 'required',
					'tanggal' => 'required',
				], $messages);

				DB::table('jadwal')->where('id', $id)->update([
					"tanggal_S"		=> strtotime($destroy[0].' '.$destroy[1]),
					"tanggal_E"		=> strtotime($destroy[3].' '.$destroy[4]),
					"keterangan"	=> $req->keterangan,
					"tanggal_M"		=> $exists->tanggal_M,
					"created_By"	=> session('auth')->nik
				]);

				$data_hitung = DB::table('peserta')->where([['id_jadwal', $id],['dispatch_level', '=', 0]])->get();

				$angka = 0;
					//kalau validasi 1, maka auto siap teknisinya
				$validasi = 1;
			}elseif(session('auth')->level == 3){
				$data_hitung = DB::table('peserta')->where([['id_jadwal', $id],['dispatch_level', '=', 1]])->get();

				$angka = 1;

				$validasi = 0;
			}

			$combine_Final = count($data_hitung);

			if($combine_Final > 0){
				foreach($data_hitung as $data){
					$array_db[] = $data->nik;
				}
				$no_match = array_diff($array_db, $nik_raw);
				if(empty($no_match)){
					$no_match = array_diff($nik_raw, $array_db);
				}
			}else{
				$no_match = $nik_raw;
			}
			// dd($no_match);

			if($count > $combine_Final){
				$hasil += $count - $combine_Final;
				$message .= 'tambah';
				if(session("auth")->level==1){
					$msg .="Admin telah menambahkan peserta untuk jadwal training tanggal ".date('Y-m-d', $exists->tanggal_S)." : \n";
					foreach($no_match as $nik){
						$myfata = DB::table('login')->where('nik', $nik)->get();
						foreach($myfata as $dd_b){
							$msg .= "$dd_b->nama ($dd_b->nik) \n";
						}
					}
					$msg.="Mohon untuk nama terlampir diatas segera melakukan konfirmasi melalui link yang teleh disediakan !\n";
					$msg.="";
					$msg.="Link : <a href='fa.serveo.net/conf/data/".session('auth')->nik."/".strtotime(date('Y-m-d H:i'))."/$id?="
					.strtotime(date('Y'))."'>Klik Disini</a>\n";
				}
				elseif(session("auth")->level==3){
					$msg .="Berikut Nama Peserta Yang Ditambah oleh TL dengan NIK ".session('auth')->nik." untuk jadwal training tanggal ".date('Y-m-d', $exists->tanggal_S)." : \n";
					foreach($no_match as $nik){
						$myfata = DB::table('login')->where('nik', $nik)->get();
						foreach($myfata as $dd_b){
							$msg .= "$dd_b->nama ($dd_b->nik) \n";
						}
					}
					$msg.="Mohon untuk nama terlampir diatas segera melakukan konfirmasi melalui link yang teleh disediakan !\n";
					$msg.="";
					$msg.="Link : <a href=\"http://fa.serveo.net/conf/data/".session('auth')->nik."/".strtotime(date('Y-m-d H:i'))."/$id?=".strtotime(date('Y'))."\"> Cek Disini </a>\n";
				}
				$this->sendTelegram($msg);

				foreach($no_match as $nik){
					DB::table('peserta')->insert([
						"id_jadwal"		=> $id,
						"dispatch_level" => $angka,
						"agreement" => $validasi,
						"nik"		=> $nik
					]);
				}
			}elseif ($count < $combine_Final) {
				$hasil += $combine_Final - $count;
				$message .= 'kurang';
				$msg .="Berikut Nama Peserta Yang Dihapus oleh TL dengan NIK ".session('auth')->nik." untuk jadwal training tanggal ".date('Y-m-d', $exists->tanggal_S)." : \n";
				foreach($no_match as $nik){
					$myfata = DB::table('login')->where('nik', $nik)->get();
					foreach($myfata as $dd_b){
						$msg .= "$dd_b->nama ($dd_b->nik) \n";
					}
				}
				$this->sendTelegram($msg);
				DB::table('peserta')->where('id_jadwal', $id)->whereIn('nik', $no_match)->delete();
			}else{
				$hasil += 0;
				$message .= 'Berhasil diupdate';
			}

			if(session('auth')->level == 1){
				$pesan = "Jadwal Untuk Tanggal $destroy[0] Hingga $destroy[3] <strong>$message $hasil Peserta!</strong>";
			}elseif(session('auth')->level == 3){
				$pesan = "di<strong>$message Sebanyak $hasil Peserta</strong> oleh TL dengan NIK ".session('auth')->nik." Untuk Jadwal Tanggal ".date('Y-m-d H:i', $exists->tanggal_S)." Hingga ".date('Y-m-d H:i', $exists->tanggal_E)."";
			}
		}else{
			$exists_date = DB::table('jadwal')->where([['tanggal_S', strtotime($destroy[0].' '.$destroy[1])],['tanggal_E', strtotime($destroy[3].' '.$destroy[4])]])->first();
			if(empty($exists_date)){
				$getid = DB::table('jadwal')->insertGetId([
					"tanggal_S"		=> strtotime($destroy[0].' '.$destroy[1]),
					"tanggal_E"		=> strtotime($destroy[3].' '.$destroy[4]),
					"keterangan"	=> $req->keterangan,
					"tanggal_M"		=> strtotime(date('Y-m-d H:i')),
					"created_By"	=> session('auth')->nik
				]);
			}else{
				$getid = $exists_date->id;
			}
			foreach($req->nik as $nik){
				DB::table('peserta')->insert([
					"id_jadwal"		=> $getid,
					"dispatch_level" => 0,
					"agreement" => 0,
					"nik"		=> $nik
				]);
			}
			$pesan = "Jadwal Untuk Tanggal $destroy[0] Hingga $destroy[3] <strong>Sudah Ditambahkan</strong> sebanyak $count Buah";
			// dd($destroy);

			$msg.= "Pengumuman!!!\n";
			$msg.= "Sebanyak $count Peserta Akan Mengikuti Pelatihan Di Fiber Academy pada : \n";
			$msg.= "Tanggal : $destroy[0]\n";
			$msg.= "Waktu : $destroy[1] \n";
			$msg.= "Bagi yang merasa Namanya ada dibawah, Harap melakukan konfirmasi ke admin.\n";
			foreach($req->nik as $nik){
				$myfata = DB::table('login')->where('nik', $nik)->get();
				foreach($myfata as $dd_b){
					$msg .= "$dd_b->nama ($dd_b->nik) \n";
				}
			}
			$msg.= "Dengan Catatan : \n";
			$msg.= "$req->keterangan.\n";
			$msg.= "Mohon untuk datang sebelum pelatihan dimulai.\n";


			$this->sendTelegram($msg);
		}

		$alert = ['type' => 'success', 'text' => $pesan];
		return redirect('/home')->with($alert);

	}
	public function sendTelegram($pesan){
		Telegram::sendMessage([
			'chat_id' => '-1001424025678',
			'parse_mode' => 'html',
			'text' => "$pesan"
		]);
	}

	public function jaddelete($id){
		// DB::table('jadwal')->leftjoin('peserta', 'jadwal.id', '=', 'peserta.id_jadwal')->where('jadwal.id', $id)->delete();
		DB::DELETE("DELETE jadwal, peserta FROM jadwal LEFT JOIN peserta ON jadwal.id = peserta.id_jadwal WHERE jadwal.id = $id");
		$alert = ['type' => 'success', 'text' => 'Jadwal Telah Berhasil Dihapus'];
		return redirect('/home')->with($alert);
	}

}
