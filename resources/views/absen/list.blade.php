@extends('layout')
@section('title')
Presensi
@endsection
@section('css')
<style type="text/css">
.label{
  margin:2px 0 2px 0;
}
</style>
@endsection
@section('content')
@if(Session::has('type'))
<ul>
  <div class="alert alert-{{session::get('type')}}">
    <li>{!! session::get('text') !!}</li>
  </div>
</ul>
@endif
<div class="demo-item" style="height: 600px;">
  <div id="testingsplig">
    <div v-for="(mydata in items">
     <strong v-text="mydata.tanggal_S"></strong>
     <div class="qrcontent" :data-id="mydata.id">
     </div>
     <div class="panel">
      <div class="panel-body">
        <div class="row" id="ontimelist">
          <div class="col-md-4 col-xs-6 label label-primary label-pill sort" v-for="(isi, key) in mydata.list_list" :key="key" v-if="isi.id_absen != null">
            <span v-text="'#'+plus(key)"></span>. <strong v-text="isi.nama"></strong><br/> 
            <strong v-text="isi.nik_peserta"></strong> ( <strong v-text="isi.date"></strong> )
          </div>
        </div>
        <div class="row" id="pembangkanglist">
          <div class="col-md-2 col-xs-6 label label-default label-pill" v-for= "data in mydata.list_list" v-if="data.id_absen == null">
            <strong v-text="data.nama"></strong> ( <strong v-text="data.nik"></strong> )
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('myjs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.8/dist/vue.js"></script>
<script src="/jquery-qrcode-master/jquery-qrcode-master/jquery.qrcode.min.js"></script>
<script>
  $(function() {
    var sudahabsen = <?= json_encode($data); ?>;
    console.log(sudahabsen.length);

    var vueData = {
      Host: $('#office').val() || 0,
      items: sudahabsen || 0
    },
    now = String($.now()),
    absen = $('.qrcontent').data("id"),
    qr='http://fa.serveo.net/absen/'+absen+'/'+now.substr(0,10);
    $('.qrcontent').html('<div class="scan"></div>');
    $('.scan').qrcode({
      width: 160,
      height: 160,
      text: qr
    });

    setInterval(function(){
      $('.scan').remove();
      now = String($.now());
      absen = $('.qrcontent').data("id");
      qr='http://fa.serveo.net/absen/'+absen+'/'+now.substr(0,10);
      $('.qrcontent').html('<div class="scan"></div>');
      $('.scan').qrcode({
        width: 160,
        height: 160,
        text: qr
      });
    }, 7000);

    var socket = io.connect('http://fa.serveo.net');
    socket.on('message', function (data) {
      console.log(data);
      data = jQuery.parseJSON(data);
      vueData.items = data;
    });

   /* new Vue({
      el: '#ontimelist',
      data: vueData,
      
    });*/

    new Vue({
      el: '#testingsplig',
      data: vueData,
      methods: {
        plus: function (numbers) {
          return numbers+1;
        }
      }
    });

/*    function sortEm(a,b){
      return parseInt($('span', a).text()) < parseInt($('span', b).text()) ? 1 : -1;
    }

    console.log(sortEm);

    $('.sort').sort(sortEm).prependTo($('#ontimelist'));

    $('[v-if="item.date"]').addClass('hide');*/
  });
</script>
@endsection