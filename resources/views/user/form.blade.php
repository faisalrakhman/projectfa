@extends('layout')
@if( Request::segment(1) == 'peserta')
@section('title', 'Tambah User')  
@elseif( Request::segment(1) == 'Team_L')
@section('title', 'Tambah Team Leader') 
@endif
@section('content')
@if(count($errors) > 0)
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<form method="post">
  <div class="form-group row">
    <div class="columnn">
      <input type="text" class="form-control form-control-user" id="nama" placeholder="Isi Nama" name="nama"
      value="{{ isset($data->nama)? $data->nama : '' }}">
    </div>
    <div class="columnn">
      <input type="text" class="form-control form-control-user" id="nik" placeholder="Isi NIK" name="nik"
      value="{{ isset($data->nik)? $data->nik : '' }}">
    </div>
    <div class="col-sm-4 select_tl">
      <select style="width: 100%;" data-allow-clear="true" class="form-control form-control-user child_select_tl" id="nik_tl" name="nik_tl">
      </select>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0">
      <input type="text" class="form-control form-control-user" id="unit" placeholder="Unit"name="unit"
      value="{{ isset($data->unit)? $data->unit : '' }}">
    </div>
    <div class="col-sm-6">
      <input type="text" class="form-control form-control-user" id="email" placeholder="Isi email" name="email"
      value="{{ isset($data->email)? $data->email : '' }}">
    </div>
  </div>
  <button type="submit" class="btn btn-success btn-block"><span class="fas fa-save 
    fa-sm text-white-50"></span> Save</button>
  </form>
  @endsection
  @section('myjs')
  <script>
    $(function() {
      var filter = '{{ Request::segment(1) }}',
      detach_select = $('.child_select_tl').detach();
      if(filter == 'peserta'){
        detach_select.appendTo( ".select_tl" );
        detach_select = null;
        $('.columnn').toggleClass( "col-md-4" );
      }else{
        detach_select = $('.child_select_tl').detach();
        $('.columnn').toggleClass( "col-md-6" );
      }

      console.log(filter);
      $('#nik_tl').select2({
        placeholder: "Masukkan NIK TL Atau Nama TL",
        minimumInputLength: 3,
        ajax: { 
          url: "/search/tl/nik",
          dataType: 'json',
          delay: 350,
          data: function (params) {
            return {
              searchTerm: params.term,
            };
          },
          processResults: function (response) {
            return {
              results: response
            };
          },
          cache: true,
          success: function(data) {
            console.log(data);
          }
        }
      });
    });
  </script>
  @endsection