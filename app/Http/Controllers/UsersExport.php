<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DB;


class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize
{ 
  use Exportable;

  public function __construct($date,$date2){
    $this->date = $date;
    $this->date2 = $date2;
  }

  public function collection()
  {
    $tgl1 = strtotime($this->date);
    $tgl2 = strtotime($this->date2);
    $fa_absen = DB::table('jadwal')->WHERE('tanggal_S', 'LIKE', '%'.$tgl1.'%')->WHERE('tanggal_E', 'LIKE', '%'.$tgl2.'%')->get();
    if(count($fa_absen) != 0){
      $cangkal = [];
      $sp2 = [];
      foreach ($fa_absen as $no => $data) {
        $peserta = DB::table('peserta')->leftjoin('save_absen', 'peserta.nik', 'save_absen.nik')->select('peserta.nik AS nik_akuh', 'peserta.*', 'save_absen.*')->where('peserta.id_jadwal', $data->id)->whereNotNull('save_absen.id')->get();
        $tidak_masuk = DB::table('peserta')->leftjoin('save_absen', 'save_absen.nik', 'peserta.nik')->select('peserta.nik AS nik_akuh', 'peserta.*', 'save_absen.*')->where('peserta.id_jadwal', $data->id)->whereNull('save_absen.id')->get(); 
        if(count($peserta) != 0 ){
          foreach ($peserta as $key => $value) {
            $cangkal[] = $value->nik_akuh;
          };
          $cangkal = implode(", ", $cangkal);
        }

        if(count($tidak_masuk) != 0 ){
          foreach ($tidak_masuk as $key => $value) {
            $sp2[] = $value->nik_akuh;
          };
          $sp2 = implode(", ", $sp2);
        }
        $mydata['nomor'] = ++$no;
        $mydata['tanggal_mulai'] = date('Y-m-d H:i', $data->tanggal_S);
        $mydata['tanggal_Berakhir'] = date('Y-m-d H:i', $data->tanggal_E);
        $mydata['peserta_rajin'] = $cangkal;
        $mydata['peserta_bebal'] = $sp2;
        $mydata['keterangan'] = $data->keterangan;
        $tes[$no] = $mydata;
      }
    }else{
     $mydata['null0'] = 'Tidak Ada Data!';
     $mydata['null1'] = 'Tidak Ada Data!';
     $mydata['null2'] = 'Tidak Ada Data!';
     $mydata['null3'] = 'Tidak Ada Data!';
     $mydata['null4'] = 'Tidak Ada Data!';
     $mydata['null5'] = 'Tidak Ada Data!';
     $tes[0] = $mydata;
   }
   return collect($tes);
 }

 public function headings(): array
 {
  return [
    'NO',
    'TANGGAL MULAI',
    'TANGGAL SELESAI',
    'PESERTA YANG HADIR',
    'PESERTA YANG TIDAK HADIR',
    'KETERANGAN',

  ];
}

}